import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(const XylophoneApp());

class XylophoneApp extends StatelessWidget {
  const XylophoneApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: displayInstrument(),
        ),
      ),
    );
  }

  Column displayInstrument() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[for (var i = 1; i < 8; i++) displayPlayPaddle(i)],
    );
  }

  Expanded displayPlayPaddle(int note) {
    const colorList = [
      Colors.green,
      Colors.red,
      Colors.blue,
      Colors.yellow,
      Colors.purple,
      Colors.pink,
      Colors.lightGreen,
    ];
    String noteName = 'note$note.wav';
    return Expanded(
      child: TextButton(
        style: TextButton.styleFrom(
          primary: colorList[note - 1],
          backgroundColor: colorList[note - 1],
        ),
        onPressed: () {
          final player = AudioCache();
          player.play(noteName);
        },
        child: const Text(
            ''), // This is necessary because TextButton must have a string
      ),
    );
  }
}
